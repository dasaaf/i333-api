const cors = require('cors');
const helmet = require('helmet');
const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const fileUpload = require('express-fileupload');
const colors = require('colors');

const winston = require('./config/Winston');
global = require('./global');

const passport = require('passport');
const LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;

const app = express();
const server = require('http').Server(app);
const tokenService = require('./controllers/tokenService');



app.use(express.json());
app.use(cors());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '10mb' }));
app.use(compression());
app.use(fileUpload());

/**
 * Secure Policy
 */
app.use(helmet());
app.use(helmet.hidePoweredBy({ setTo: 'i333api' }));
app.use(helmet.ieNoOpen());
app.use(helmet.xssFilter());
app.use(passport.initialize());
app.use(passport.session());

const UserLoginLinkedinController = require('./controllers/UserLoginLinkedinController');

passport.use(new LinkedInStrategy({
  clientID: global.LINKEDIN_API_KEY,
  clientSecret: global.LINKEDIN_SECRET_KEY,
  callbackURL: global.LINKEDIN_CALLBACK,
  scope: ['r_emailaddress', 'r_liteprofile'],
}, (async (accessToken, refreshToken, profile, done) => {
  // asynchronous verification, for effect...
  // console.log(colors.green('accestoken'), accessToken);
  // console.log(colors.red('profile'), JSON.stringify(profile));
  /* UserLoginLinkedinController.action(profile, (err, theUser) => {
    process.nextTick(() => done(null, theUser));
  }); */
  const user = await UserLoginLinkedinController.action(profile);
  process.nextTick(() => done(null, user));
})));

passport.serializeUser((theUser, done) => {
  done(null, theUser);
});

passport.deserializeUser((theUser, done) => {
  done(null, theUser);
});

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Access-Control-Expose-Headers', 'token');
  next();
});

app.get('/auth/linkedin/callback', passport.authenticate('linkedin', { failureRedirect: '/login' }), (req, res) => {
  const userRegistered = req.user;
  if (userRegistered) {
    const token = tokenService.createToken(userRegistered);
    return res.redirect(`${global.PAGE_SUCCESS_REDIRECT}/#/login?access=${token}`);
  }
  return res.redirect(`${global.PAGE_SUCCESS_REDIRECT}/#/login?access=null`);
});

const routes = require('./routes');

app.use('/api', routes);

/**
 * Error handler.
 */
app.use((err, req, res, next) => {
  // Internal errors
  if (!err.status) {
    err.status = 500;
  }

  const content = {
    statusCode: err.status,
    endpoint: req.originalUrl,
    method: req.method,
    message: err.message,
    headers: req.headers,
    body: req.body,
  };

  if (err.status >= 400 && err.status < 500) {
    winston.info(err, content);
  }
  if (err.status >= 500) {
    winston.error(err, content);
  }

  res.status(err.status || 500);
  res.json({
    message: err.message,
    errors: err.errors || {},
    status: err.status,
  }).end();
});

module.exports = app;
