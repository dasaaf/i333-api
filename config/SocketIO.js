const redisConfig = {
  host: 'localhost',
  port: 6379,
  db: 0,
};

const socketIO = require('socket.io');
const socketIORedis = require('socket.io-redis');
const redis = require('redis').createClient(redisConfig);

const redisAdapter = socketIORedis(redisConfig);

/**
 *
 * @param socket
 * @param name
 * @param clients
 * @param data
 */
function eventToClients(socket, name, clients, data) {
  const filteredClients = clients.filter((n) => n != null);
  redis.mget(filteredClients, (error, socketIds) => {
    if (!socketIds || error) {
      return;
    }
    socketIds.forEach((socket_id) => { socket.to(socket_id).emit(name, { data }); });
  });
}

/**
 *
 * @param socket
 * @param name
 * @param client
 * @param data
 */
function eventToClient(socket, name, client, data) {
  redis.get(client, (error, socketId) => {
    if (!socketId || error) {
      return;
    }
    socket.to(socketId).emit(name, { data });
  });
}

function init(server) {
  console.log('entroxxx');
  const io = socketIO(server);
  io.adapter(redisAdapter);
  io.sockets.on('connection', (socket) => {
    socket.on('disconnect', () => {
      if (socket.localid) {
        redis.del(socket.localid);
      }
    });

    socket.on('joinroom', (data) => {
      console.log('entro', data);
      socket.localid = data.user_id;
      socket.join(data.user_id);
      redis.set(data.user_id, socket.id);
    });

    socket.on('message.client', (data) => {
      eventToClient(socket, data.name, data.client, data.payload);
    });

    socket.on('message.clients', (data) => {
      eventToClients(socket, data.name, data.clients, data.payload);
    });
  });
}

module.exports = init;
