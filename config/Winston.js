'use strict';

const winston = require('winston');

const options = {
  console: {
    level: process.env.LEVEL_LOG || 'error',
    handleExceptions: true,
    json: true,
    stringify: (obj) => JSON.stringify(obj),
    colorize: true,
    timestamp: function () {
      return new Date();
    },
  },
};

// eslint-disable-next-line new-cap
const logger = new winston.createLogger({
  level: 'info',
  transports: [new winston.transports.Console(options.console)],
  exitOnError: false,
});

logger.stream = {
  write: function (message, encoding) {
    logger.info(message);
  },
};

module.exports = logger;
