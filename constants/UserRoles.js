const ENTREPRENEUR = 'ENTREPRENEUR';

class UserRoles {

  /**
   *
   * @returns {String}
   */
  static get entrepreneurRole() {
    return ENTREPRENEUR;
  }

}

module.exports = UserRoles;
