const BUCKET = 'testing-i333';
const USER_DIR = 'users/';
const USER_DIR_DEFAULT = 'users/default/';
const USER_IMG_PROFILE_DEFAULT = '4b660a17-e529-4bf8-b773-6c33508ae88b.jpg';
const PROJECT_DIR = 'projects/';
const PROJECT_DIR_DEFAULT = 'projects/default/';
const PROJECT_IMG_DEFAULT = '970de01a-182b-4049-af8e-902ed6eb2ed7.jpg';

class S3Constants {

  /**
   *
   * @returns {string}
   */
  static get bucket() {
    return BUCKET;
  }

  /**
   *
   * @returns {string}
   */
  static get userDir() {
    return USER_DIR;
  }

  /**
   *
   * @returns {string}
   */
  static get userDirDefault() {
    return USER_DIR_DEFAULT;
  }

  /**
   *
   * @returns {string}
   */
  static get userImgProfileDefault() {
    return USER_IMG_PROFILE_DEFAULT;
  }

  /**
   *
   * @returns {string}
   */
  static get projectDir() {
    return PROJECT_DIR;
  }

  /**
   *
   * @returns {string}
   */
  static get projectDirDefault() {
    return PROJECT_DIR_DEFAULT;
  }

  /**
   *
   * @returns {string}
   */
  static get projectImgDefault() {
    return PROJECT_IMG_DEFAULT;
  }

}

module.exports = S3Constants;
