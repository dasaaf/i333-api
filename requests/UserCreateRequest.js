const BaseRequest = require('./BaseRequest');

class UserCreateRequest extends BaseRequest {
  /**
   *
   * @returns {{first_name: string, email: string}}
   */
  rules() {
    const rules = {
      email: 'required|string|email',
      first_name: 'required|string|alpha',
    };
    return rules;
  }
}

module.exports = UserCreateRequest;
