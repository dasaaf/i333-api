const BaseRequest = require('./BaseRequest');

class PageQueryRequest extends BaseRequest {
  /**
   *
   * @returns {{name: string}}
   */
  rules() {
    const rules = {
      page: 'numeric|min:1',
    };
    return rules;
  }

  /**
   *
   * @returns {Promise<*>}
   */
  dataValidate() {
    return this.request.query;
  }
}

module.exports = PageQueryRequest;
