const BaseRequest = require('./BaseRequest');

class MongoIdParamRequest extends BaseRequest {
  /**
   *
   * @returns {{name: string}}
   */
  rules() {
    const rules = {
      id: 'required|string|mongo_id',
    };
    return rules;
  }

  /**
   *
   * @returns {Promise<*>}
   */
  dataValidate() {
    return this.request.params;
  }
}

module.exports = MongoIdParamRequest;
