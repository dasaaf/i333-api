const validator = require('validator');
const ValidatorJS = require('validatorjs');
const HttpCodes = require('../controllers/HttpCodes');

class RequestValidator {
  /**
   *
   * @param data
   * @param rules
   * @returns {Promise<void>}
   */
  static async check(data, rules) {
    const errors = await this.validate(data, rules);
    return errors;
  }

  /**
   *
   * @param data
   * @param rules
   * @returns {Promise<void>}
   */
  static async validate(data, rules) {
    this.moreRules();
    const validation = new ValidatorJS(data, rules);
    return new Promise((resolve) => {
      validation.fails(() => {
        const { errors } = validation.errors;
        const key = Object.keys(errors)[0];
        const error = new Error(validation.errors.first(key));
        error.status = HttpCodes['Bad Request'];
        error.errors = errors;
        return resolve(error);
      });

      validation.passes(() => resolve(null));
    });
  }

  static async moreRules() {
    ValidatorJS.registerAsync('mongo_id', (data, attribute, req, passes) => {
      if (typeof data !== 'string' || !data.match(/^[0-9a-fA-F]{24}$/)) {
        return passes(false, 'invalid format');
      }
      return passes();
    });
  }
}

module.exports = RequestValidator;
