const RequestValidator = require('./RequestValidator');

class BaseRequest {
  /**
   *
   * @param request
   * @param response
   * @param next
   */
  constructor(request, response, next) {
    this.request = request;
    this.response = response;
    this.next = next;
  }

  dataValidate() {
    return this.request.body;
  }

  async check() {
    const error = await RequestValidator.check(this.dataValidate(), this.rules());
    if (error) {
      return this.next(error);
    }
    return this.next();
  }

  /**
   * implements in child classes
   */
  rules() {
  }
}

module.exports = BaseRequest;
