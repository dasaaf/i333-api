const BaseRequest = require('../BaseRequest');

class CreateRequest extends BaseRequest {
  /**
   *
   * @returns {{name: string}}
   */
  rules() {
    const rules = {
      user_receiver: 'required|string|mongo_id',
      content: 'required|string',
    };
    return rules;
  }
}

module.exports = CreateRequest;
