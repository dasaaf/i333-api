const BaseRequest = require('./BaseRequest');

class MongoUserIdParamRequest extends BaseRequest {
  /**
   *
   * @returns {{name: string}}
   */
  rules() {
    const rules = {
      user_id: 'required|string|mongo_id',
    };
    return rules;
  }

  /**
   *
   * @returns {Promise<*>}
   */
  dataValidate() {
    return this.request.params;
  }
}

module.exports = MongoUserIdParamRequest;
