const BaseRequest = require('../BaseRequest');

class CreateRequest extends BaseRequest {
  /**
   *
   * @returns {{name: string}}
   */
  rules() {
    const rules = {
      name: 'required|string|in:EXPERT',
    };
    return rules;
  }
}
module.exports = CreateRequest;
