const mongoose = require('mongoose');

const { Schema } = mongoose;

const SubscriptionSchema = Schema({
  id: String,
  duration: String,
  price: Number,
  discount: Number,
  state: String,
  created_date: Date,
  updated_date: Date,
  deleted_date: Date,
  user: { type: Schema.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Subscriptions', SubscriptionSchema);
