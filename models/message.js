const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const { Schema } = mongoose;

const MessageSchema = Schema({
  id: String,
  content: String,
  created_date: Date,
  updated_date: Date,
  deleted_date: Date,
  received_date: Date,
  read_date: Date,
  files: [Array],
  user_sender: { type: Schema.ObjectId, ref: 'User' },
  user_receiver: { type: Schema.ObjectId, ref: 'User' },
});

MessageSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Messages', MessageSchema);
