const mongoose = require('mongoose');

const { Schema } = mongoose;

const UserSchema = Schema({
  id: String,
  photo: String,
  email: String,
  first_name: String,
  last_name: String,
  birth_date: Date,
  title: String,
  professional_profile: String,
  academic_training: Object,
  skills: Object,
  laboral_experiences: Object,
  certificates: Array,
  references: Object,
  password: String,
  password_recovery: String,
  state: String,
  role: Array,
  created_date: Date,
  updated_date: Date,
  deleted_date: Date,
});

module.exports = mongoose.model('Users', UserSchema);
