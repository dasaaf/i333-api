const mongoose = require('mongoose');

const { Schema } = mongoose;

const ProjectSchema = Schema({
  id: String,
  project_image: String,
  title: String,
  description: String,
  needs: String,
  locations: Object,
  swot_matrix: Object,
  business_model_canvas: Object,
  state: String,
  created_date: Date,
  updated_date: Date,
  deleted_date: Date,
  user: { type: Schema.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Projects', ProjectSchema);
