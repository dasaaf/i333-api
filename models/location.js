const csc = require('country-state-city');

class LocationModel {
  getAllCountries() {
    const countries = csc.default.getAllCountries();
    return countries;
  }

  getCountry(countryId) {
    const country = csc.default.getCountryById(countryId);
    return country;
  }

  getStatesOfCountry(countryId) {
    const states = csc.default.getStatesOfCountry(countryId);
    return states;
  }

  getState(stateId) {
    const state = csc.default.getStateById(stateId);
    return state;
  }

  getCitiesOfState(stateId) {
    const cities = csc.default.getCitiesOfState(stateId);
    return cities;
  }

  getCity(cityId) {
    const city = csc.default.getCityById(cityId);
    return city;
  }
}
module.exports = LocationModel;
