const mongoose = require('mongoose');

const { Schema } = mongoose;

const ReportSchema = Schema({
  id: String,
  description: String,
  created_date: Date,
  id_negotiation: { type: Schema.ObjectId, ref: 'Negotiation' },
});

module.exports = mongoose.model('Reports', ReportSchema);
