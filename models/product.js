const mongoose = require('mongoose');

const { Schema } = mongoose;

const ProductSchema = Schema({
  id: String,
  product_images: Array,
  name: String,
  description: String,
  type: String,
  step_duration: String,
  duration: String,
  price: Number,
  discount: Number,
  sales: Number,
  orders: Number,
  state: String,
  published: Boolean,
  created_date: Date,
  updated_date: Date,
  deleted_date: Date,
  project: { type: Schema.ObjectId, ref: 'Project' },
});

module.exports = mongoose.model('Products', ProductSchema);
