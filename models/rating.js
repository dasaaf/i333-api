const mongoose = require('mongoose');

const { Schema } = mongoose;

const RatingSchema = Schema({
  id: String,
  qualification: Number,
  created_date: Date,
  updated_date: Date,
  deleted_date: Date,
  grader: { type: Schema.ObjectId, ref: 'User' },
  expert: { type: Schema.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Ratings', RatingSchema);
