const mongoose = require('mongoose');

const { Schema } = mongoose;

const RequestRoleSchema = Schema({
  id: String,
  state: String, // PENDING,APPROVED, REJECTED
  role_name: String, // EXPERT
  review: String,
  created_date: Date,
  updated_date: Date,
  deleted_date: Date,
  user_id: { type: Schema.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('RequestRoles', RequestRoleSchema);
