const mongoose = require('mongoose');

const { Schema } = mongoose;

const NegotiationSchema = Schema({
  id: String,
  state: String,
  created_date: Date,
  updated_date: Date,
  deleted_date: Date,
  project: { type: Schema.ObjectId, ref: 'Project' },
  interested_user: { type: Schema.ObjectId, ref: 'User' },
  property_user: { type: Schema.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Negotiations', NegotiationSchema);
