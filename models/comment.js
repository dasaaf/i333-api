const mongoose = require('mongoose');

const { Schema } = mongoose;

const CommentSchema = Schema({
  id: String,
  state: String,
  description: String,
  created_date: Date,
  updated_date: Date,
  deleted_date: Date,
  commentator: { type: Schema.ObjectId, ref: 'User' },
  expert: { type: Schema.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Comments', CommentSchema);
