const passport = require('passport');
const LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;

const LINKEDIN_CLIENT_ID = 'CLIENT_ID_HERE';
const LINKEDIN_CLIENT_SECRET = 'CLIENT_SECRET_HERE';
const Linkedin = require('node-linkedin')(LINKEDIN_CLIENT_ID, LINKEDIN_CLIENT_SECRET);

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((obj, done) => {
  done(null, obj);
});

passport.use(new LinkedInStrategy({
  clientID: LINKEDIN_CLIENT_ID,
  clientSecret: LINKEDIN_CLIENT_SECRET,
  callbackURL: 'http://127.0.0.1:5001/user/auth/linkedin/callback',
  scope: ['r_emailaddress', 'r_basicprofile', 'rw_company_admin'],
  passReqToCallback: true,
},
((req, accessToken, refreshToken, profile, done) => {
  req.session.accessToken = accessToken;
  process.nextTick(() => done(null, profile));
})));
