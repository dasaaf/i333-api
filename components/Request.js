const axios = require('axios');

class Request {
  /**
   *
   * @param fullResponse
   * @param url
   * @param headers
   * @returns {Promise<void>}
   */
  async getImageExternalServer(fullResponse, url, headers) {
    const responseType = 'arraybuffer';
    const response = await this.makeRequest(fullResponse, 'get', url, null, headers, responseType);
    return response;
  }

  /**
   *
   * @param fullResponse
   * @param url
   * @param headers
   * @param responseType
   * @returns {Promise<*>}
   */
  async GET(fullResponse, url, headers, responseType = null) {
    const response = await this.makeRequest(fullResponse, 'get', url, null, headers, responseType);
    return response;
  }

  /**
   *
   * @param fullResponse
   * @param url
   * @param json
   * @param headers
   * @param responseType
   * @returns {Promise<*>}
   */
  async POST(fullResponse, url, json, headers, responseType = null) {
    const response = await this.makeRequest(fullResponse, 'post', url, json, headers, responseType);
    return response;
  }

  /**
   *
   * @param fullResponse
   * @param method
   * @param url
   * @param payload
   * @param headers
   * @param responseType
   * @returns {Promise<*>}
   */
  async makeRequest(fullResponse, method, url, payload = null, headers = null, responseType = null) {
    const options = {
      method,
      url,
    };
    if (payload !== null) {
      options.data = payload;
    }
    if (headers) {
      options.headers = headers;
    }
    if (responseType) {
      options.responseType = responseType;
    }
    const response = await this.send(fullResponse, options);
    return response;
  }

  /**
   * @param fullResponse
   * @param options
   * @returns {Response | Error}
   */
  async send(fullResponse, options) {
    try {
      const response = await axios(options);
      const { data } = response;
      if (fullResponse === true) {
        return response;
      }
      return data;
    } catch (error) {
      throw error;
    }
  }

}
module.exports = new Request();
