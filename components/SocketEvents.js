const SocketIOEmitter = require('./SocketIOEmitter');

class SocketEvents {
  /**
   *
   * @param from user sender user_id
   * @param to user receiver
   * @param message
   */
  static sendMessageToUser(from, to, message) {
    const event = 'chat';
    const payload = {
      from,
      message,
    };
    SocketIOEmitter.messageToClient(event, to, payload);
  }
}

module.exports = SocketEvents;
