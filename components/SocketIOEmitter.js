const global = require('../global');

// TODO :: change localhost by env value use wss in production
const socketEmitter = require('socket.io-client')(`ws://localhost:${global.SERVER_PORT}`, { transports: ['websocket'] });

class SocketIOEmitter {

  /**
   *
   * @param name
   * @param client
   * @param payload
   */
  static messageToClient(name, client, payload) {
    const content = { name, client, payload };
    this.send('message.client', content);
  }

  /**
   *
   * @param name
   * @param clients
   * @param payload
   */
  static messageToClients(name, clients, payload) {
    const content = { name, clients, payload };
    this.send('message.clients', content);
  }

  /**
   *
   * @param event
   * @param data
   */
  static send(event, data) {
    socketEmitter.emit(event, data);
  }

}

module.exports = SocketIOEmitter;
