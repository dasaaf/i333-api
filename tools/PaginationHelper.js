const customLabels = {
  docs: 'data',
  totalDocs: 'total',
};

const defaultLimit = 5;

module.exports = {
  customLabels,
  defaultLimit,
};
