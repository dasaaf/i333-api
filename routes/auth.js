const express = require('express');
const AuthController = require('../controllers/auth');

const api = express.Router();

api.post('/authenticate', AuthController.login);

module.exports = api;
