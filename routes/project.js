const express = require('express');
const multipart = require('connect-multiparty');
const ProjectController = require('../controllers/project');
const authenticatedMiddleware = require('./authenticatedMiddleware');

const multipartMiddleware = multipart({ uploadDir: './uploads/projects/' });

const api = express.Router();

api.get('/search/project/:text/:records_number?/:page?/:sort_field?/:sort_order?', authenticatedMiddleware.ensureAuthenticated, ProjectController.searchProject);
api.get('/projects/:user?/:records_number?/:page?/:sort_field?/:sort_order?', authenticatedMiddleware.ensureAuthenticated, ProjectController.getProjects);
api.get('/count_projects/:user?', authenticatedMiddleware.ensureAuthenticated, ProjectController.getCountProjects);
api.get('/project/:id', authenticatedMiddleware.ensureAuthenticated, ProjectController.getProject);
api.get('/project/image/:id', authenticatedMiddleware.ensureAuthenticated, ProjectController.getFilePath);
api.post('/project', authenticatedMiddleware.ensureAuthenticated, ProjectController.createProject);
api.put('/project/:id', authenticatedMiddleware.ensureAuthenticated, ProjectController.updateProject);
api.delete('/project/:id', authenticatedMiddleware.ensureAuthenticated, ProjectController.deleteProject);

module.exports = api;
