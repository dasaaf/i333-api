const express = require('express');

const router = express.Router();

router.use(require('./auth'));

router.use(require('./comment'));

router.use(require('./investment'));

router.use(require('./location'));

router.use(require('./message'));

router.use(require('./negotiation'));

router.use(require('./product'));

router.use(require('./project'));

router.use(require('./rating'));

router.use(require('./report'));

router.use(require('./roleRequest'));

router.use(require('./subscription'));

router.use(require('./user'));

module.exports = router;
