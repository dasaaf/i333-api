const express = require('express');
const CommentController = require('../controllers/comment');
const authenticatedMiddleware = require('./authenticatedMiddleware');

const api = express.Router();

api.post('/comment', authenticatedMiddleware.ensureAuthenticated, CommentController.createComment);

api.get('/comments/:expert_id', authenticatedMiddleware.ensureAuthenticated, CommentController.getComments);

module.exports = api;
