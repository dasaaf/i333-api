const multipart = require('connect-multiparty');
const express = require('express');

const ProductController = require('../controllers/product');

const api = express.Router();

const multipartMiddleware = multipart({ uploadDir: './uploads/products/' });
const authenticatedMiddleware = require('./authenticatedMiddleware');

api.get('/search/product/:text/:records_number?/:page?/:sort_field?/:sort_order?', ProductController.searchProduct);
api.get('/products/:project?/:records_number?/:page?/:sort_field?/:sort_order?', ProductController.getProducts);
api.get('/product/:id', ProductController.getProduct);
api.get('/product/image/:id/:image_name?', ProductController.getFilePath);
api.post('/product', authenticatedMiddleware.ensureAuthenticated, multipartMiddleware, ProductController.createProduct);
api.put('/product/:id', authenticatedMiddleware.ensureAuthenticated, multipartMiddleware, ProductController.updateProduct);
api.put('/product/publish/:userId/:productId', authenticatedMiddleware.ensureAuthenticated, ProductController.publishProduct);
api.delete('/product/:id', authenticatedMiddleware.ensureAuthenticated, ProductController.deleteProduct);

module.exports = api;
