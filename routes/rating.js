const express = require('express');
const RatingController = require('../controllers/rating');

const api = express.Router();
const authenticatedMiddleware = require('./authenticatedMiddleware');

api.get('/rating/:expert_id/:grader_id?', authenticatedMiddleware.ensureAuthenticated, RatingController.getRating);
api.post('/rating', authenticatedMiddleware.ensureAuthenticated, RatingController.createRating);
api.put('/rating/:rating_id', authenticatedMiddleware.ensureAuthenticated, RatingController.updateRating);

module.exports = api;
