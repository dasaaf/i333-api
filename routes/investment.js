const express = require('express');
const InvestmentController = require('../controllers/investment');

const api = express.Router();

api.get('/investment/:id', InvestmentController.getInvestment);
api.get('/investments/:investor_user', InvestmentController.getInvestments);
api.post('/investment', InvestmentController.createInvestment);
api.put('/investment/:id', InvestmentController.updateInvestment);
api.delete('/investment/:id', InvestmentController.deleteInvestment);

module.exports = api;
