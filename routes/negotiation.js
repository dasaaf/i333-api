const express = require('express');

const api = express.Router();

const NegotiationController = require('../controllers/negotiation');

const authenticatedMiddleware = require('./authenticatedMiddleware');

api.get('/negotiation/:id', authenticatedMiddleware.ensureAuthenticated, NegotiationController.getNegotiation);
api.get('/negotiations/:user/:project?/:page?/:records_number?/:sort_field?/:sort_order?', authenticatedMiddleware.ensureAuthenticated, NegotiationController.getNegotiations);
api.post('/negotiation', authenticatedMiddleware.ensureAuthenticated, NegotiationController.createNegotiation);

module.exports = api;
