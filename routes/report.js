const multipart = require('connect-multiparty');
const express = require('express');

const api = express.Router();
const ReportController = require('../controllers/report');

const multipartMiddleware = multipart({ uploadDir: './uploads/projects/' });
const authenticatedMiddleware = require('./authenticatedMiddleware');

api.get('/reports/:negotiation_id?/:records_number?/:page?/:sort_field?/:sort_order?', authenticatedMiddleware.ensureAuthenticated, ReportController.getReports);
api.get('/report/:id', authenticatedMiddleware.ensureAuthenticated, ReportController.getReport);
api.post('/report', authenticatedMiddleware.ensureAuthenticated, multipartMiddleware, ReportController.createReport);

module.exports = api;
