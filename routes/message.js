const express = require('express');

const router = express.Router();

const MessageController = require('../controllers/message');

const { controller, request } = require('./Resolver');

// TODO :: change to new structure middleware
const authenticatedMiddleware = require('./authenticatedMiddleware');

router.post('/message',
  authenticatedMiddleware.ensureAuthenticated,
  request('Message/CreateRequest'),
  controller('Message/CreateController'));

router.get('/message/:id',
  authenticatedMiddleware.ensureAuthenticated,
  request('MongoIdParamRequest'),
  controller('Message/ShowController'));

router.put('/message/:id/received',
  authenticatedMiddleware.ensureAuthenticated,
  request('MongoIdParamRequest'),
  controller('Message/ReceivedController'));

router.put('/message/:id/read',
  authenticatedMiddleware.ensureAuthenticated,
  request('MongoIdParamRequest'),
  controller('Message/ReadController'));

router.get('/messages/history/with/:user_id',
  authenticatedMiddleware.ensureAuthenticated,
  request('MongoUserIdParamRequest'),
  request('PageQueryRequest'),
  controller('Message/HistoryBetweenUsersController'));

// TODO :: check with request.user the user_id
router.get('/convertations/:user_id',
  authenticatedMiddleware.ensureAuthenticated,
  request('MongoUserIdParamRequest'),
  MessageController.getConvertations);

router.get('/message/file/:file_name',
  authenticatedMiddleware.ensureAuthenticated,
  MessageController.getFilePath);

router.put('/message/accept/:id',
  authenticatedMiddleware.ensureAuthenticated,
  request('MongoIdParamRequest'),
  MessageController.acceptMessage);

module.exports = router;
