const express = require('express');

const router = express.Router();

const { controller, request } = require('./Resolver');

// TODO :: change to new structure middleware
const authenticatedMiddleware = require('./authenticatedMiddleware');

router.post('/role-request',
  authenticatedMiddleware.ensureAuthenticated,
  request('RoleRequest/CreateRequest'),
  controller('RoleRequest/CreateController'));

module.exports = router;
