/**
 *
 * @param fn
 * @returns {Function}
 */
const wrapper = (fn) => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next);
};

/**
 *
 * @param namespace
 * @param method
 * @returns {Function}
 */
const resolver = (namespace, method) => wrapper(async (req, res, next) => {
  // eslint-disable-next-line global-require,import/no-dynamic-require
  const ClassName = require(`../${namespace}`);
  // eslint-disable-next-line no-return-await
  return await new ClassName(req, res, next)[method]();
});

/**
 *
 * @param requestName
 * @param method
 * @returns {Function}
 */
const request = (requestName, method = 'check') => resolver(`requests/${requestName}`, method);

/**
 *
 * @param controllerName
 * @param method
 * @returns {Function}
 */
const controller = (controllerName, method = 'action') => resolver(`controllers/${controllerName}`, method);

/**
 *
 * @param middlewareName
 * @param method
 * @returns {Function}
 */
const middleware = (middlewareName, method = 'action') => resolver(`middleware/${middlewareName}`, method);

module.exports = {
  request,
  controller,
  middleware,
};
