const express = require('express');
const SubscriptionController = require('../controllers/subscription');
const authenticatedMiddleware = require('./authenticatedMiddleware');

const api = express.Router();

api.get('/subscription/:user_id', authenticatedMiddleware.ensureAuthenticated, SubscriptionController.getSubscription);
api.post('/subscription', authenticatedMiddleware.ensureAuthenticated, SubscriptionController.createSubscription);

module.exports = api;
