const express = require('express');
const authenticatedMiddleware = require('./authenticatedMiddleware');
const Location = require('../controllers/location');

const api = express.Router();

api.get('/location/countries', authenticatedMiddleware.ensureAuthenticated, Location.getAllCountries);
api.get('/location/country/:countryId', authenticatedMiddleware.ensureAuthenticated, Location.getCountry);
api.get('/location/states/:countryId', authenticatedMiddleware.ensureAuthenticated, Location.getStates);
api.get('/location/state/:stateId', authenticatedMiddleware.ensureAuthenticated, Location.getState);
api.get('/location/cities/:stateId', authenticatedMiddleware.ensureAuthenticated, Location.getCities);
api.get('/location/city/:cityId', authenticatedMiddleware.ensureAuthenticated, Location.getCity);

module.exports = api;
