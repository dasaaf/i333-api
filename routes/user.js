const express = require('express');
const multipart = require('connect-multiparty');
const UserController = require('../controllers/user');
const UserLoginController = require('../controllers/UserLoginController');

const api = express.Router();

const multipartMiddleware = multipart({ uploadDir: './uploads/users/' });
const authenticatedMiddleware = require('./authenticatedMiddleware');

api.get('/user/:id', authenticatedMiddleware.ensureAuthenticated, UserController.getUser);
api.get('/experts/:records_number?/:page?/:sort_field?/:sort_order?', authenticatedMiddleware.ensureAuthenticated, UserController.getExperts);
api.get('/search/expert/:text/:records_number?/:page?/:sort_field?/:sort_order?', authenticatedMiddleware.ensureAuthenticated, UserController.searchExpert);
api.get('/user/photo/:id', authenticatedMiddleware.ensureAuthenticated, UserController.getFilePath);
api.get('/activate-user/:key/:id', UserController.activateAccount);
api.post('/user', multipartMiddleware, UserController.createUser);
api.post('/verify-recaptcha', UserController.verifyRecaptcha);
api.post('/upload-certificate/:id', UserController.uploadCertificate);
api.delete('/remove-certificate/:id_certificate', UserController.removeCertificate);
api.get('/download-certificate', UserController.downloadCertificate);
api.put('/user/reset-password', UserController.resetPassword);
api.put('/user/password-recovery', UserController.passwordRecovery);
// api.put('/user/:id', authenticatedMiddleware.ensureAuthenticated, multipartMiddleware, UserController.updateUser);
api.put('/user/:id', authenticatedMiddleware.ensureAuthenticated, UserController.updateUser);
api.delete('/user/:id', authenticatedMiddleware.ensureAuthenticated, UserController.deleteUser);

api.get('/me', authenticatedMiddleware.ensureAuthenticated, UserLoginController.me);

module.exports = api;
