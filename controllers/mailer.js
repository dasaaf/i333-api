const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const fse = require('fs-extra');
const path = require('path');
const handlebars = require('handlebars');

const config = require('../global');

function sendMail(email, link, type_mail) {

  let subject_message = '';
  let body_template = '';

  if (type_mail === config.REGISTER_MAIL) {
    subject_message = '¡Hola! - Bienvenido a i333';
    const source = fse.readFileSync(path.join(__dirname, '../views/templates/welcome.hbs'), 'utf8');
    body_template = handlebars.compile(source);
  }

  else if (type_mail === config.RESET_PASSWORD_MAIL) {
    subject_message = 'i333 - Solicitud de cambio de clave';
    const source = fse.readFileSync(path.join(__dirname, '../views/templates/reset_password.hbs'), 'utf8');
    body_template = handlebars.compile(source);
  }

  if (type_mail === config.REGISTER_MAIL || type_mail === config.RESET_PASSWORD_MAIL) {
    const transporter = nodemailer.createTransport(smtpTransport({
      service: 'gmail',
      secure: false, // use SSL
      auth: {
        user: config.MAIL_ACCOUNT_USER,
        pass: config.MAIL_ACCOUNT_PASSWORD,
      },
      tls: {
        rejectUnauthorized: false,
      },
    }));

    const mailOptions = {
      from: config.MAIL_ACCOUNT_USER,
      to: email,
      subject: subject_message,
      html: body_template({link})
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      }
    });
  }

}

module.exports = {
  sendMail,
};
