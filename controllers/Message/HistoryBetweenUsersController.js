const BaseController = require('../BaseController');
const UserRepository = require('../../repositories/UserRepository');
const GenericException = require('../../exceptions/GenericException');
const MessageRepository = require('../../repositories/MessageRepository');

class HistoryBetweenUsersController extends BaseController {
  /**
   *
   * @return {Promise<void>}
   */
  async action() {
    const { params } = this.request;
    const { query } = this.request;
    const userIdA = this.request.user;
    const user = await UserRepository.findById(userIdA);
    if (!user) {
      throw new GenericException('the user sender does not exists');
    }
    const userIdB = params.user_id;
    const messages = await MessageRepository.historyBetweenUsersWithPagination(
      userIdA, userIdB, query.page,
    );
    return this.success(messages);
  }
}

module.exports = HistoryBetweenUsersController;
