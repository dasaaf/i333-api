const BaseController = require('../BaseController');
const MessageRepository = require('../../repositories/MessageRepository');

class ShowController extends BaseController {
  /**
   *
   * @returns {Promise<*>}
   */
  async action() {
    const { params } = this.request;
    const { id } = params;
    const message = await MessageRepository.findById(id);
    if (!message) {
      return this.notFound();
    }
    return this.success({ message });
  }
}

module.exports = ShowController;
