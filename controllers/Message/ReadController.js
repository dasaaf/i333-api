const moment = require('moment');
const BaseController = require('../BaseController');
const GenericException = require('../../exceptions/GenericException');
const MessageRepository = require('../../repositories/MessageRepository');

class ReadController extends BaseController {
  /**
   *
   * @return {Promise<void>}
   */
  async action() {
    const { params } = this.request;
    const { id } = params;
    const message = await MessageRepository.findById(id);
    if (!message) {
      return this.notFound();
    }
    if (message.read_date) {
      throw new GenericException('The message was read');
    }
    message.read_date = moment.now();
    await message.save();
    return this.noContent();
  }
}

module.exports = ReadController;
