const moment = require('moment');
const BaseController = require('../BaseController');
const GenericException = require('../../exceptions/GenericException');
const MessageRepository = require('../../repositories/MessageRepository');

class ReceivedController extends BaseController {
  /**
   *
   * @return {Promise<void>}
   */
  async action() {
    const { params } = this.request;
    const { id } = params;
    const message = await MessageRepository.findById(id);
    if (!message) {
      return this.notFound();
    }
    if (message.received_date) {
      throw new GenericException('The message was received');
    }
    message.received_date = moment.now();
    await message.save();
    return this.noContent();
  }
}

module.exports = ReceivedController;
