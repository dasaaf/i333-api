const BaseController = require('../BaseController');
const SocketEvents = require('../../components/SocketEvents');
const GenericException = require('../../exceptions/GenericException');
const UserRepository = require('../../repositories/UserRepository');
const MessageRepository = require('../../repositories/MessageRepository');

class CreateController extends BaseController {
  /**
   *
   * @return {Promise<*>}
   */
  async action() {
    const input = this.request.body;
    const userId = this.request.user;
    const userSender = await UserRepository.findById(userId);
    if (!userSender) {
      throw new GenericException('the user sender does not exists');
    }
    if (userSender.deleted_date) {
      throw new GenericException('the user sender is deleted');
    }
    const userReceiver = await UserRepository.findById(input.user_receiver);
    if (!userReceiver) {
      throw new GenericException('the user receiver does not exists');
    }
    if (userReceiver.deleted_date) {
      throw new GenericException('the user receiver is deleted');
    }
    const message = await MessageRepository.create(
      // eslint-disable-next-line no-underscore-dangle
      userSender._id,
      // eslint-disable-next-line no-underscore-dangle
      userReceiver._id,
      input.content,
      null,
    );
    // eslint-disable-next-line no-underscore-dangle
    SocketEvents.sendMessageToUser(userSender._id, userReceiver._id, message);
    return this.created(message);
  }
}

module.exports = CreateController;
