const mongoose = require('mongoose');
const Rating = require('../models/rating');

function ratingResponse(expertId, graderId, res) {
  Rating.find({ expert: expertId, grader: graderId }).exec((err, rating) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can\'t complete the operation.',
      });
    } else if (!rating) {
      res.status(404).send({
        message: 'Comments not found',
      });
    } else {
      res.status(200).send({
        rating,
      });
    }
  });
}

function scoreResponse(expertId, res) {
  Rating.aggregate([
    { $match: { expert: mongoose.Types.ObjectId(expertId) } },
    { $group: { _id: null, score: { $avg: '$qualification' } } },
  ], (err, rating) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can\'t complete the operation.',
      });
    } else if (!rating) {
      res.status(404).send({
        message: 'Comments not found',
      });
    } else {
      res.status(200).send({
        rating,
      });
    }
  });
}

function getRating(req, res) {
  const expertId = req.params.expert_id;
  const graderId = req.params.grader_id;
  if (graderId) {
    ratingResponse(expertId, graderId, res);
  } else {
    scoreResponse(expertId, res);
  }
}

function createRating(req, res) {
  const rating = new Rating();
  const params = req.body;
  rating.qualification = params.qualification;
  rating.expert = params.expert;
  rating.grader = params.grader;
  rating.created_date = new Date();
  rating.updated_date = new Date();
  rating.deleted_date = null;
  rating.save((err, ratingSaved) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: 'Error: the rating can not was registered',
        error: err,
      });
    } else {
      res.status(200).send({
        rating: ratingSaved,
      });
    }
  });
}

function updateRating(req, res) {
  const ratingId = req.params.rating_id;
  const params = req.body;
  Rating.findByIdAndUpdate(ratingId, params, { new: true }, (err, rating) => {
    if (err) {
      res.status(500).send({
        message: 'Error: The rating don\'t save',
      });
    } else if (!rating) {
      res.status(404).send({
        message: 'Rating not exits!',
      });
    } else {
      res.status(200).send({
        rating,
      });
    }
  });
}

module.exports = {
  createRating,
  getRating,
  updateRating,
};
