const LocationModel = require('../models/location');

const location = new LocationModel();

function getAllCountries(req, res) {
  const countries = location.getAllCountries();
  if (countries) {
    res.json(countries);
  } else {
    res.status(400).send({
      message: 'Countries not found',
    });
  }
}

function getCountry(req, res) {
  const { countryId } = req.params;
  const country = location.getCountry(countryId);
  if (country) {
    res.json(country);
  } else {
    res.status(400).send({
      message: 'States not found',
    });
  }
}

function getStates(req, res) {
  const { countryId } = req.params;
  const states = location.getStatesOfCountry(countryId);
  if (states) {
    res.json(states);
  } else {
    res.status(400).send({
      message: 'States not found',
    });
  }
}

function getState(req, res) {
  const { stateId } = req.params;
  const state = location.getState(stateId);
  if (state) {
    res.json(state);
  } else {
    res.status(400).send({
      message: 'State not found',
    });
  }
}

function getCities(req, res) {
  const { stateId } = req.params;
  const cities = location.getCitiesOfState(stateId);
  if (cities) {
    res.json(cities);
  } else {
    res.status(400).send({
      message: 'Cities not found',
    });
  }
}

function getCity(req, res) {
  const { cityId } = req.params;
  const city = location.getCity(cityId);
  if (city) {
    res.json(city);
  } else {
    res.status(400).send({
      message: 'City not found',
    });
  }
}

module.exports = {
  getAllCountries,
  getStates,
  getState,
  getCities,
  getCity,
  getCountry,
};
