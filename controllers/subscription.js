const Subscription = require('../models/subscription');

function getSubscription(req, res) {
  const userId = req.params.user_id;
  Subscription.find({ user: userId }, (err, subscription) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can\'t complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!subscription) {
      res.status(404).send({
        message: {
          text: 'Subscription not exists!',
          code: 404,
          error: '',
        },
      });
    } else {
      res.status(200).send({
        subscription,
      });
    }
  });
}

function createSubscription(req, res) {
  const subscription = new Subscription();
  const params = req.body;
  subscription.duration = params.duration;
  subscription.price = params.price;
  subscription.discount = params.discount;
  subscription.state = 'ACTIVE';
  subscription.user = params.user;
  subscription.created_date = new Date();
  subscription.updated_date = new Date();
  subscription.deleted_date = null;

  subscription.save((err, subscriptionSaved) => {
    if (err) {
      console.error('error', err);
      res.status(500).send({
        message: 'Error: the Subscription can not was registered',
        error: err,
      });
    } else {
      res.status(200).send({
        subscription: subscriptionSaved,
      });
    }
  });

}

module.exports = {
  getSubscription,
  createSubscription,
};
