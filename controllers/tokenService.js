const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../global');

exports.createToken = (user) => {
  const payload = {
    sub: user._id,
    iat: moment().unix(),
    exp: moment().add(15, 'days').unix(),
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
};
