/* eslint-disable dot-notation */

const HttpCodes = require('./HttpCodes');

class Api {
  /**
     *
     * @param response
     */
  static noContent(response) {
    response.statusCode = HttpCodes['No Content'];
    response.end();
  }

  /**
     *
     * @param response
     */
  static notFound(response) {
    response.statusCode = HttpCodes['Not Found'];
    response.end();
  }

  /**
     *
     * @param response
     * @param object
     */
  static objectCreated(response, object) {
    this.responseJson(response, object, HttpCodes['Created']);
  }

  /**
     *
     * @param response
     * @param object
     */
  static success(response, object) {
    this.responseJson(response, object, HttpCodes['OK']);
  }

  /**
     *
     * @param response
     * @param error
     */
  static internalError(response, error) {
    response.statusCode = HttpCodes['Internal Server Error'];
    response.send(error.message);
    response.end();
  }

  /**
     *
     * @param response
     * @param error
     */
  static badRequest(response, error) {
    this.responseJson(response, error, HttpCodes['Bad Request']);
  }

  /**
     *
     * @param response
     * @param object
     * @param code
     */
  static responseJson(response, object, code) {
    response.statusCode = code;
    response.json(object);
    response.end();
  }
}

module.exports = Api;
