const { v4: uuidv4 } = require('uuid');

const User = require('../models/user');
const UserRoles = require('../constants/UserRoles');
const ImageFromExternalUrl = require('../services/ImageFromExternalUrl');
const S3Constants = require('../constants/S3Constants');
const fileManager = require('../services/fileManager');

class UserLoginLinkedinController {
  /**
   *
   * @param content
   */
  async action(content) {
    const email = content.emails[0].value;
    const firstName = content.name.givenName;
    const lastName = content.name.familyName;
    const userRegistered = await User.findOne({ email });
    if (!userRegistered) {
      const user = new User();
      user.first_name = firstName;
      user.last_name = lastName;
      user.photo = `${S3Constants.userDirDefault}${S3Constants.userImgProfileDefault}`;
      user.email = email;
      user.title = 'UNDEFINED';
      user.professional_profile = 'UNDEFINED';
      user.academic_training = [];
      user.skills = [];
      user.laboral_experiences = [];
      user.password = null;
      user.password_recovery = '';
      user.state = 'ACTIVE';
      user.role = UserRoles.entrepreneurRole;
      user.birth_date = null;
      user.created_date = new Date();
      user.updated_date = new Date();
      user.deleted_date = null;
      const userSaved = await user.save();
      // check if the user has default photo, update for linkedin profile photo
      const newPhotoURL = content.photos[3].value;
      const newUser = await this.updatePhoto(userSaved.photo, newPhotoURL, userSaved);
      return newUser;
    }
    // user registered
    userRegistered.first_name = firstName;
    userRegistered.last_name = lastName;
    userRegistered.email = email;
    userRegistered.state = 'ACTIVE';
    // check if the user has default photo, update for linkedin profile photo
    const newPhotoURL = content.photos[3].value;
    const user = await this.updatePhoto(userRegistered.photo, newPhotoURL, userRegistered);
    return user;
  }

  /**
   *
   * @param currentPhotoUrl
   * @param newPhotoURL
   * @param user
   */
  async updatePhoto(currentPhotoUrl, newPhotoURL, user) {
    const defaultPhoto = `${S3Constants.userDirDefault}${S3Constants.userImgProfileDefault}`;
    if (currentPhotoUrl === defaultPhoto) {
      const image = await ImageFromExternalUrl.upload(newPhotoURL);
      // eslint-disable-next-line no-underscore-dangle
      const pathImage = `${S3Constants.userDir}${user._id}/${uuidv4()}.${image.extension}`;
      const objectS3 = await fileManager.put(S3Constants.bucket, pathImage, image.data);
      // update the photo is storage is success and remove
      if (objectS3) {
        await fileManager.remove(S3Constants.bucket, user.photo);
        user.photo = pathImage;
      }
      const userWithPhotoUpdated = await user.save();
      return userWithPhotoUpdated;
    }
    return user;
  }

  /**
   *
   * @param content
   * @param callback
   */
  callbackAction(content, callback) {
    const email = content.emails[0].value;
    User.findOne({ email }, (err, userRegistered) => callback(err, userRegistered));
  }
}

module.exports = new UserLoginLinkedinController();
