const User = require('../models/user');
const crypto = require('../tools/crypto');
const const_states = require('../global');
const tokenService = require('./tokenService');

function login(req, res) {
  const input = req.body;
  const { email, password } = input;
  User.findOne({ email }, (err, user) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!user) {
      res.status(404).send({
        message: {
          text: 'User not found',
          code: 404,
          error: '',
        },
      });
    } else if (!user.password) {
      res.status(404).send({
        message: {
          text: 'You must recover your password',
          code: 404,
          error: '',
        },
      });
    } else if (user.state !== const_states.ACTIVE) {
      res.status(202).send({
        message: {
          text: 'Please, activate your account from the mail sended',
        },
      });
    } else if (crypto.decrypt(user.password) === password) {
      res.set('token', tokenService.createToken(user));
      res.status(200).send({
        user,
      });
    } else {
      res.status(202).send({
        message: {
          text: 'User or password is not valid',
        },
      });
    }
  });
}

module.exports = {
  login,
};
