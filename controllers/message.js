const mongoose = require('mongoose');
const fs = require('fs');

const Message = require('../models/message');
const Negotiation = require('../models/negotiation');

function getConvertations(req, res) {
  const { user_id } = req.params;
  Message.aggregate([
    { $match: { $or: [
      { user_sender: mongoose.Types.ObjectId(user_id) },
      { user_receiver: mongoose.Types.ObjectId(user_id) },
    ] } },
    { $group: {
      _id: { user_sender: '$user_sender', user_receiver: '$user_receiver' },
      created_date: { $last: '$created_date' },
    } },
    { $sort: { created_date: -1 } },
  ], (err, messages) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can not complete the operation.',
      });
    } else if (!messages) {
      res.status(404).send({
        message: 'Messages not found',
      });
    } else {
      res.status(200).send({
        messages,
      });
    }
  });
}

function acceptMessage(req, res) {
  const { id } = req.params;
  Message.findByIdAndUpdate(id, { state: 'ACCEPTED' }, (err, message) => {
    if (err) {
      res.status(500).send({
        message: 'Error: The project do not save',
      });
    } else {
      Negotiation.findByIdAndUpdate(message.negotiation, { state: 'INVESTMENT' }, (err, negotiation) => {
        if (err) {
          res.status(500).send({
            message: 'Error: The project do not save',
          });
        } else {
          res.status(200).send({
            message,
          });
        }
      });
    }
  });
}

function getFilePath(req, res) {
  const { file_name } = req.params;

  fs.exists(`./uploads/messages/files attached/${file_name}`, (exists) => {
    if (exists) {

      const file = fs.createReadStream(`./uploads/messages/files attached/${file_name}`);
      const stat = fs.statSync(`./uploads/messages/files attached/${file_name}`);

      res.setHeader('Content-Length', stat.size);
      res.setHeader('Content-Type', 'application/pdf');
      res.setHeader('Content-Disposition', 'attachment; filename=quote.pdf');
      file.pipe(res);
      const chunks = [];
      res.on('data', (chunk) => {
        console.log('downloading');
        chunks.push(chunk);
      });
      res.on('end', () => {
        console.log('downloaded');
        const jsfile = new Buffer.concat(chunks).toString('base64');
        console.log('converted to base64');
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'X-Requested-With');
        res.header('content-type', 'application/pdf');
        res.send(jsfile);
      });
    } else {
      res.status(404).send({
        message: '¡File not found!. ',
      });
    }
  });

}

module.exports = {
  getConvertations,
  getFilePath,
  acceptMessage,
};
