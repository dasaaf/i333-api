const BaseController = require('../BaseController');
const GenericException = require('../../exceptions/GenericException');
const UserRepository = require('../../repositories/UserRepository');
const RequestRoleRepository = require('../../repositories/RequestRoleRepository');

class CreateController extends BaseController {
  /**
   *
   * @returns {Promise<*>}
   */
  async action() {
    const input = this.request.body;
    const userId = this.request.user;
    const user = await UserRepository.findById(userId);
    if (!user) {
      throw new GenericException('the user does not exists');
    }
    if (user.deleted_date) {
      throw new GenericException('the user is deleted');
    }
    if (user.role.indexOf('EXPERT') !== -1) {
      throw new GenericException('the user is expert');
    }
    const requestRolePending = await RequestRoleRepository.findPending(
      userId,
      input.name,
    );
    if (requestRolePending) {
      throw new GenericException('the user has a request role pending');
    }
    const requestRole = await RequestRoleRepository.create('PENDING', userId, input.name);
    return this.created(requestRole);
  }
}

module.exports = CreateController;
