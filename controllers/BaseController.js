const Api = require('./Api');

class BaseController {
  /**
   *
   * @param request
   * @param response
   */
  constructor(request, response) {
    this.request = request;
    this.response = response;
  }

  /**
   *
   * @param response
   * @param error
   * @returns {*}
   */
  processError(response, error) {
    if (error && error.isInternal) {
      return this.badRequest(error);
    }
    return this.internalError(error);
  }

  /**
   *
   * @returns {*}
   */
  noContent() {
    return this.responseWithoutData('noContent');
  }

  /**
   *
   * @returns {*}
   */
  methodNotAllowed() {
    const object = { message: 'The resource is not yet available' };
    return this.responseWithData('methodNotAllowed', object);
  }

  /**
   *
   * @param object
   * @returns {*}
   */
  success(object) {
    return this.responseWithData('success', object);
  }

  /**
   *
   * @param error
   * @returns {*}
   */
  internalError(error) {
    return this.responseWithData('internalError', error);
  }

  /**
   *
   * @param object
   * @returns {*}
   */
  created(object) {
    return this.responseWithData('objectCreated', object);
  }

  /**
   *
   * @param error
   * @returns {*}
   */
  badRequest(error) {
    return this.responseWithData('badRequest', error);
  }

  /**
   * @returns {*}
   */
  notFound() {
    return this.responseWithoutData('notFound');
  }

  /**
   * @returns {*}
   */
  forbidden() {
    return this.responseWithoutData('forbidden');
  }

  /**
   *
   * @param apiMethod
   * @private
   */
  // eslint-disable-next-line no-underscore-dangle
  responseWithoutData(apiMethod) {
    return Api[apiMethod](this.response);
  }

  /**
   *
   * @param apiMethod
   * @param data
   * @private
   */
  responseWithData(apiMethod, data) {
    return Api[apiMethod](this.response, data);
  }
}

module.exports = BaseController;
