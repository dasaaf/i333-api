const User = require('../models/user');
const Comment = require('../models/comment');
const const_states = require('../global');

function getComments(req, res) {
  const expert_id = req.params.expert_id;
  let sort_field = 'created_date';
  let sort_order = 'desc';
  Comment.find({ expert: expert_id }).sort([[sort_field, sort_order]]).exec((err, comments) => {
    if (err) {
      res.status(500).send({
        message: `Error: It is was can't complete the operation.`,
      });
    } else if (!comments) {
      res.status(404).send({
        message: 'Comments not found',
      });
    } else {
      User.populate(comments, { path: 'commentator', select: { password: 0 } }, (err, commentsAndUser) => {
        if (err) {
          res.status(500).send({
            message: `Error: It is was can't complete the operation!. Not exist the user`,
          });
        } else {
          res.status(200).send({
            comments: commentsAndUser,
          });
        }
      });
    }
  });
}

function createComment(req, res) {
  const comment = new Comment();
  const params = req.body;
  comment.description = params.description;
  comment.state = const_states.POSTED;
  comment.commentator = params.commentator;
  comment.expert = params.expert;
  comment.created_date = new Date();
  comment.updated_date = new Date();
  comment.deleted_date = null;
  comment.save((err, commentSaved) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: `Error: the comment can not was registered`,
        error: err,
      });
    } else {
      res.status(200).send({
        comment: commentSaved,
      });
    }
  });
}

module.exports = {
  createComment,
  getComments,
};
