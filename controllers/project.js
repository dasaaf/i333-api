const path = require('path');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

const Project = require('../models/project');
const User = require('../models/user');
const S3Constants = require('../constants/S3Constants');
const fileManager = require('../services/fileManager');

function getProject(req, res) {
  const { id } = req.params;
  Project.findById({ _id: id, state: { $ne: 'DELETED' } }, (err, project) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!project) {
      res.status(404).send({
        message: {
          text: 'Project not exists!',
          code: 404,
          error: '',
        },
      });
    } else if (project.state !== 'DELETED') {
      res.status(200).send({
        project,
      });
    } else {
      res.status(200).send({
        project: null,
      });
    }
  });
}

function getCountProjects(req, res) {
  const userId = req.params.user;
  let find;
  if (!userId) {
    find = Project.count({});
  } else {
    find = Project.countDocuments({ user: userId });
  }
  find.exec((err, c) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can not complete the operation.',
      });
    } else {
      res.status(200).send({
        count: c,
      });
    }
  });
}

function getProjects(req, res) {
  const userId = req.params.user;
  const records_number = parseInt(req.params.records_number);
  const page = parseInt(req.params.page);
  const s = (page - 1) * records_number;
  let { sort_field } = req.params;
  let { sort_order } = req.params;
  let find;
  if (!userId || userId === 'null') {
    find = Project.find({ state: { $ne: 'DELETED' } });
  } else {
    find = Project.find({ user: userId, state: { $ne: 'DELETED' } });
  }
  if (!sort_field) {
    sort_field = 'created_date';
  }
  if (!sort_order) {
    sort_order = 'desc';
  }
  find.exec((err, results) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can not complete the operation!. Not exist the user',
      });
    } else {
      find.limit(records_number)
        .skip(s)
        .sort([[sort_field, sort_order]])
        .exec((errorFindingProject, projects) => {
          if (errorFindingProject) {
            res.status(500).send({
              message: 'Error: It is was can not complete the operation.',
            });
          } else if (!projects) {
            res.status(404).send({
              message: 'Projects not found',
            });
          } else {
            User.populate(projects, { path: 'user', select: { password: 0 } }, (err, project) => {
              if (err) {
                res.status(500).send({
                  message: 'Error: It is was can not complete the operation!. Not exist the user',
                });
              } else {
                const pages_number = Math.ceil(results.length / records_number);
                res.status(200).send({
                  project,
                  pages_number,
                });
              }
            });
          }
        });
    }
  });
}

function createProject(req, res) {
  const project = new Project();
  const params = req.body;
  project.title = params.title;
  //project.project_image = req.files && req.files.files != null ? req.files.files.name : 'default_logo.jpg';
  project.project_image = S3Constants.projectDirDefault + S3Constants.projectImgDefault;
  project.description = params.description;
  project.needs = params.needs;
  project.swot_matrix = params.swot_matrix ? JSON.parse(params.swot_matrix) : {};
  project.business_model_canvas = params.business_model_canvas ? JSON.parse(params.business_model_canvas) : {};
  project.state = params.state;
  project.user = params.user;
  project.created_date = new Date();
  project.updated_date = new Date();
  project.deleted_date = null;
  if (params.locations) {
    const loc = new Array();
    params.locations.forEach((e) => {
      loc.push(JSON.parse(e));
    });
    project.locations = loc;
  }

  project.save((err, projectSaved) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: 'Error: the project can not was registered',
        error: err,
      });
    } else {
      res.status(200).send({
        project: projectSaved,
      });
    }
  });

}

function updateProject2(req, res) {
  const { id } = req.params;
  const params = req.body;
  Project.findById(id, (err, project) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!project) {
      res.status(404).send({
        message: {
          text: 'Project not exists!',
          code: 404,
          error: '',
        },
      });
    } else {
      if (req.files && req.files.files && req.files.files.path && project.project_image == "default_logo.jpg") {
        params.project_image = req.files.files.path.split('\\')[2];
      } else {
        params.project_image = project.project_image;
      }
      params.updated_date = new Date();
      if (params.swot_matrix) {
        params.swot_matrix = JSON.parse(params.swot_matrix);
      }
      if (params.business_model_canvas) {
        params.business_model_canvas = JSON.parse(params.business_model_canvas);
      }
      if (params.locations) {
        const loc = new Array();
        if (Array.isArray(params.locations)) {
          params.locations.forEach((e) => {
            loc.push(JSON.parse(e));
          });
        } else {
          loc.push(JSON.parse(params.locations));
        }
        params.locations = loc;
      } else {
        params.locations = [];
      }
      Project.findByIdAndUpdate(id, params, { new: true }, (err, projectUpdated) => {
        if (err) {
          res.status(500).send({
            message: 'Error: The project don\'t save',
          });
        } else if (!projectUpdated) {
          res.status(404).send({
            message: 'Project not exits!',
          });
        } else {
          if (req.files && req.files.files && project.project_image != 'default_logo.jpg') {

            fs.unlink(`./uploads/projects/${project.project_image}`, (err) => {
              if (err) throw err;
            });
            fs.rename(`./uploads/projects/${req.files.files.path.split('\\')[2]}`, `./uploads/projects/${project.project_image}`, (err) => {
              if (err) console.log(`ERROR: ${err}`);
            });

          }
          res.status(200).send({
            project: projectUpdated,
          });
        }
      });
    }
  });
}

async function updateProject(req, res) {
  try {
    const { id } = req.params;
    const params = req.body;
    const project = await Project.findById(id);
    if (project) {
      params.updated_date = new Date();
      if (params.swot_matrix) {
        params.swot_matrix = JSON.parse(params.swot_matrix);
      }
      if (params.business_model_canvas) {
        params.business_model_canvas = JSON.parse(params.business_model_canvas);
      }
      if (params.locations) {
        const loc = new Array();
        if (Array.isArray(params.locations)) {
          params.locations.forEach((e) => {
            loc.push(JSON.parse(e));
          });
        } else {
          loc.push(JSON.parse(params.locations));
        }
        params.locations = loc;
      } else {
        params.locations = [];
      }
      const projectUpdated = await Project.findByIdAndUpdate(id, params, { new: true });
      if (projectUpdated) {
        if (req.files.files) {
          const arrImage = req.files.files.name.split('.');
          const extImage = arrImage[arrImage.length - 1];
          const pathImage = `${S3Constants.projectDir + req.params.id}/${uuidv4()}.${extImage}`;
          const objectS3 = await fileManager.put(S3Constants.bucket, pathImage, req.files.files.data);
          if (objectS3) {
            if (projectUpdated.project_image !== (S3Constants.projectDirDefault + S3Constants.projectImgDefault)) {
              await fileManager.remove(S3Constants.bucket, projectUpdated.project_image);
            }
            projectUpdated.project_image = pathImage;
          }
        }
        await Project.findByIdAndUpdate(id, projectUpdated);
        res.status(200).send({
          user: projectUpdated,
        });
      }
    } else {
      res.status(404).send({
        message: {
          text: 'User not exists!',
          code: 404,
          error: '',
        },
      });
    }
  } catch (e) {
    console.log(e);
    res.send(e);
  }
}

function deleteProject(req, res) {
  const { id } = req.params;
  const deleted_date = new Date();
  Project.findByIdAndUpdate(id, { state: 'DELETED', deleted_date }, (err, projectDeleted) => {
    if (err) {
      res.status(500).send({
        message: 'Error: The project don\'t save',
      });
    } else if (!projectDeleted) {
      res.status(404).send({
        message: 'Project not exits!',
      });
    } else {
      res.status(200).send({
        project: projectDeleted,
      });
    }
  });
}

function getFilePath2(req, res) {
  const project_id = req.params.id;
  Project.findById(project_id, (err, project) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!project) {
      res.status(404).send({
        message: {
          text: 'Project not exists!',
          code: 404,
          error: '',
        },
      });
    } else {

      fs.exists(`./uploads/projects/${project.project_image}`, (exists) => {
        if (exists) {
          res.sendFile(path.resolve(`./uploads/projects/${project.project_image}`));
        } else {
          res.status(404).send({
            message: '¡File not found!. ',
          });
        }
      });

    }
  });
}

async function getFilePath(req, res) {
  try {
    const project_id = req.params.id;
    const project = await Project.findById(project_id);
    const image = await fileManager.get(S3Constants.bucket, project.project_image);
    res.send(image.Body);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
}

function searchProject(req, res) {
  const { text } = req.params;
  const records_number = parseInt(req.params.records_number);
  const page = parseInt(req.params.page);
  const s = (page - 1) * records_number;
  let { sort_field } = req.params;
  let { sort_order } = req.params;
  if (!sort_field) {
    sort_field = 'created_date';
  }
  if (!sort_order) {
    sort_order = 'desc';
  }
  const filter = {
    $or: [
      { title: { $regex: text, $options: 'i' } },
      { description: { $regex: text, $options: 'i' } },
    ],
    $and: [{ state: { $ne: 'DELETED' } }],
  };
  const query = Project.find(filter);
  query.limit(records_number).skip(s).sort([[sort_field, sort_order]]).exec((err, project) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!project) {
      res.status(404).send({
        message: {
          text: 'Project not exists!',
          code: 404,
          error: '',
        },
      });
    } else {
      res.status(200).send({
        project,
      });
    }
  });
}

module.exports = {
  getProject,
  getCountProjects,
  getProjects,
  getFilePath,
  createProject,
  updateProject,
  deleteProject,
  searchProject,
};
