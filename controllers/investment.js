const Investment = require('../models/investment');
const const_states = require('../global');

function getInvestment(req, res) {
  const investment = req.params.id;
  Investment.find({ _id: investment }).exec((err, investments) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can\'t complete the operation.',
      });
    } else if (!investments) {
      res.status(404).send({
        message: 'Investment not found',
      });
    } else {
      res.status(200).send({
        investments,
      });
    }
  });
}

function getInvestments(req, res) {
  const { investor_user } = req.params;
  Investment.find({ investor_user }).exec((err, investments) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can\'t complete the operation.',
      });
    } else if (!investments) {
      res.status(404).send({
        message: 'Investment not found',
      });
    } else {
      res.status(200).send({
        investments,
      });
    }
  });
}

function createInvestment(req, res) {
  const investment = new Investment();
  const params = req.body;
  investment.project = params.project;
  investment.investor_user = params.investor_user;
  investment.state = const_states.NEGOTIATION;
  investment.created_date = new Date();
  investment.updated_date = new Date();
  investment.deleted_date = null;
  investment.save((err, investmentSave) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: 'Error: the investment can not was registered',
        error: err,
      });
    } else {
      res.status(200).send({
        investment: investmentSave,
      });
    }
  });
}

function updateInvestment(req, res) {
}

function deleteInvestment(req, res) {

}

module.exports = {
  getInvestment,
  getInvestments,
  createInvestment,
  updateInvestment,
  deleteInvestment,
};
