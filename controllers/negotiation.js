const Negotiation = require('../models/negotiation');
const Project = require('../models/project');
const User = require('../models/user');
const const_states = require('../global');

function getNegotiation(req, res) {
  const negotiation = req.params.id;
  Negotiation.find({ _id: negotiation }).exec((err, negotiation) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can not complete the operation.',
      });
    } else if (!negotiation) {
      res.status(404).send({
        message: 'Negotiation not found',
      });
    } else {
      res.status(200).send({
        negotiation,
      });
    }
  });
}

function getNegotiations(req, res) {
  const { user } = req.params;
  const { project } = req.params;
  const { page } = req.params;
  const records_number = parseInt(req.params.records_number);
  const s = (page - 1) * records_number;
  let { sort_field } = req.params;
  let { sort_order } = req.params;
  if (!sort_field) {
    sort_field = 'created_date';
  }
  if (!sort_order) {
    sort_order = 'desc';
  }
  let query;
  if (project && project != 'null') {
    query = { interested_user: user, project };
  } else {
    query = { $or: [{ interested_user: user }, { property_user: user }] };
  }
  Negotiation.find(query).exec((err, results) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can\'t complete the operation.',
      });
    } else {
      Negotiation.find(query).limit(records_number).skip(s).exec((err, negotiations) => {
        if (err) {
          res.status(500).send({
            message: 'Error: It is was can\'t complete the operation.',
          });
        } else if (!negotiations) {
          res.status(404).send({
            message: 'Negotiations not found',
          });
        } else {
          Project.populate(negotiations, { path: 'project' }, (err, negotiationsList) => {
            if (err) {
              res.status(500).send({
                message: 'Error: It is was can\'t complete the operation!. Not exist the project',
              });
            } else {
              User.populate(negotiationsList, [{ path: 'interested_user', select: { password: 0 } }, { path: 'property_user', select: { password: 0 } }], (err, negotiationsListAndUsers) => {
                if (err) {
                  res.status(500).send({
                    message: 'Error: It is was can\'t complete the operation!. Not exist the user',
                  });
                } else {
                  const pages_number = Math.ceil(results.length / records_number);
                  res.status(200).send({
                    negotiations: negotiationsListAndUsers,
                    pages_number,
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

function createNegotiation(req, res) {
  const negotiation = new Negotiation();
  const params = req.body;
  negotiation.project = params.project;
  negotiation.interested_user = params.interested_user;
  negotiation.property_user = params.property_user;
  negotiation.state = const_states.NEGOTIATION;
  negotiation.created_date = new Date();
  negotiation.updated_date = new Date();
  negotiation.deleted_date = null;
  negotiation.save((err, negotiationSave) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: 'Error: the negotiation can not was registered',
        error: err,
      });
    } else {
      User.populate(negotiationSave, [{ path: 'interested_user', select: { password: 0 } }, { path: 'property_user', select: { password: 0 } }], (err, negotiationAndUserSaved) => {
        if (err) {
          res.status(500).send({
            message: 'Error: It is was can\'t complete the operation!. Not exist the user',
          });
        } else {
          res.status(200).send({
            negotiation: negotiationAndUserSaved,
          });
        }
      });
    }
  });
}

module.exports = {
  getNegotiation,
  getNegotiations,
  createNegotiation,
};
