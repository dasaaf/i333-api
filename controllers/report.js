const Report = require('../models/report');
const Negotiation = require('../models/negotiation');

function getReport(req, res) {
  const { id } = req.params;
  Report.findById(id, (err, report) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!report) {
      res.status(404).send({
        message: {
          text: 'Report not exists!',
          code: 404,
          error: '',
        },
      });
    } else {
      res.status(200).send({
        report,
      });
    }
  });
}

function getReports(req, res) {
  const negotiationId = req.params.negotiation;
  const records_number = parseInt(req.params.records_number);
  const page = parseInt(req.params.page);
  const s = (page - 1) * records_number;
  let { sort_field } = req.params;
  let { sort_order } = req.params;
  let find;
  if (!negotiationId || negotiationId == 'null') {
    find = Report.find({});
  } else {
    find = Report.find({ negotiation: negotiationId });
  }
  if (!sort_field) {
    sort_field = 'created_date';
  }
  if (!sort_order) {
    sort_order = 'desc';
  }
  find.exec((err, results) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can not complete the operation!. Not exist the negotiation',
      });
    } else {
      find.limit(records_number).skip(s).sort([[sort_field, sort_order]]).exec((err, reports) => {
        if (err) {
          res.status(500).send({
            message: 'Error: It is was can not complete the operation.',
          });
        } else if (!reports) {
          res.status(404).send({
            message: 'Reports not found',
          });
        } else {
          Negotiation.populate(reports, { path: 'negotiation' }, (err, report) => {
            if (err) {
              res.status(500).send({
                message: 'Error: It is was can not complete the operation!. Not exist the negotiation',
              });
            } else {
              const pages_number = Math.ceil(results.length / records_number);
              res.status(200).send({
                report,
                pages_number,
              });
            }
          });
        }
      });
    }
  });
}

function createReport(req, res) {
  const report = new Report();
  const params = req.body;
  report.description = params.description;
  report.negotiation = params.negotiation;
  report.created_date = new Date();

  report.save((err, reportSaved) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: 'Error: the report can not was registered',
        error: err,
      });
    } else {
      res.status(200).send({
        report: reportSaved,
      });
    }
  });
}

module.exports = {
  getReport,
  getReports,
  createReport,
};
