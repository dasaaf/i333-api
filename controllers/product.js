const path = require('path');
const fs = require('fs');
const Product = require('../models/product');
const Project = require('../models/project');
const Subscription = require('../models/subscription');

function getProduct(req, res) {
  const { id } = req.params;
  Product.findById({ _id: id, state: { $ne: 'DELETED' } }, (err, product) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!Product) {
      res.status(404).send({
        message: {
          text: 'Product not exists!',
          code: 404,
          error: '',
        },
      });
    } else {
      res.status(200).send({
        Product: product,
      });
    }
  });
}

function getProducts(req, res) {
  const projectId = req.params.project;
  const records_number = parseInt(req.params.records_number);
  const page = parseInt(req.params.page);
  const s = (page - 1) * records_number;
  let { sort_field } = req.params;
  let { sort_order } = req.params;
  let find;
  if (!projectId || projectId === 'null') {
    find = Product.find({ state: { $ne: 'DELETED' } });
  } else {
    find = Product.find({ project: projectId, state: { $ne: 'DELETED' } });
  }
  if (!sort_field) {
    sort_field = 'created_date';
  }
  if (!sort_order) {
    sort_order = 'desc';
  }
  find.exec((err, results) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can not complete the operation!. Not exist the user',
      });
    } else {
      find.limit(records_number).skip(s).sort([[sort_field, sort_order]]).exec((err, products) => {
        if (err) {
          res.status(500).send({
            message: 'Error: It is was can not complete the operation.',
          });
        } else if (!products) {
          res.status(404).send({
            message: 'Products not found',
          });
        } else {
          Project.populate(products, { path: 'project' }, (err, product) => {
            if (err) {
              res.status(500).send({
                message: 'Error: It is was can not complete the operation!. Not exist the user',
              });
            } else {
              const pages_number = Math.ceil(results.length / records_number);
              res.status(200).send({
                Product: product,
                PagesNumber: pages_number,
              });
            }
          });
        }
      });
    }
  });
}

function createProduct(req, res) {
  const product = new Product();
  const params = req.body;
  let imagesNames = new Array();
  imagesNames.push('default_product.jpg');
  for (const key in req.files) {
    const d = new Date();
    const day = d.getDate().toString();
    const month = d.getMonth().toString();
    const year = d.getFullYear().toString();
    const hours = d.getHours().toString();
    const minutes = d.getMinutes().toString();
    const seconds = d.getSeconds().toString();
    const newName = `${req.files[key].path.split('\\')[2].split('.')[0]} ${day}-${month}-${year} ${hours}-${minutes}-${seconds}.jpg`;
    
    fs.rename(`./uploads/products/${req.files[key].path.split('\\')[2]}`, `./uploads/products/${newName}`, (err) => {
      if (err) console.log(`ERROR: ${err}`);
    });
    
    if (imagesNames[0] == 'default_product.jpg') {
      imagesNames = new Array();
      imagesNames.push(newName);
    } else {
      imagesNames.push(newName);
    }
  }
  product.product_images = imagesNames;
  product.name = params.name;
  product.description = params.description;
  product.type = params.type;
  product.step_duration = params.step_duration;
  product.duration = params.duration;
  product.price = params.price;
  product.discount = params.discount;
  product.sales = params.sales;
  product.orders = params.orders;
  product.state = 'ACTIVE';
  product.published = false;
  product.project = params.project;
  product.created_date = new Date();
  product.updated_date = new Date();
  product.deleted_date = null;

  product.save((err, productSaved) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: 'Error: the Product can not was registered',
        error: err,
      });
    } else {
      res.status(200).send({
        user: productSaved,
      });
    }
  });
}

function updateProduct(req, res) {
  const { id } = req.params;
  const params = req.body;
  Product.findById(id, (err, product) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!Product) {
      res.status(404).send({
        message: {
          text: 'Product not exists!',
          code: 404,
          error: '',
        },
      });
    } else {
      const imagesNames = new Array();
      for (const key in req.files) {
        const d = new Date();
        const day = d.getDate().toString();
        const month = d.getMonth().toString();
        const year = d.getFullYear().toString();
        const hours = d.getHours().toString();
        const minutes = d.getMinutes().toString();
        const seconds = d.getSeconds().toString();
        const newName = `${req.files[key].path.split('\\')[2].split('.')[0]} ${day}-${month}-${year} ${hours}-${minutes}-${seconds}.jpg`;
        
        fs.rename(`./uploads/products/${req.files[key].path.split('\\')[2]}`, `./uploads/products/${newName}`, (err) => {
          if (err) console.log(`ERROR: ${err}`);
        });
        
        imagesNames.push(newName);
      }
      product.product_images.forEach((image) => {
        if (image != 'default_product.jpg') {
          /*
          fs.unlink(`./uploads/products/${image}`, (err) => {
            if (err) console.log(`ERROR: ${err}`);
          });
          */
        }
      });
      params.product_images = imagesNames;
      params.updated_date = new Date();
      update(id, params, res);
    }
  });
}

function deleteProduct(req, res) {
  const productId = req.params.id;
  const delete_date = new Date();
  Product.findByIdAndUpdate(productId, { state: 'DELETED', delete_date }, (err, productDeleted) => {
    if (err) {
      res.status(500).send({
        message: 'Error: The product don\'t save',
      });
    } else if (!productDeleted) {
      res.status(404).send({
        message: 'Product not exits!',
      });
    } else {
      res.status(200).send({
        product: productDeleted,
      });
    }
  });
}

function getFilePath(req, res) {
  const product_id = req.params.id;
  const { image_name } = req.params;
  if (image_name) {
    sendImagePath(image_name, res);
  } else {
    findImageById(product_id, res);
  }
}

function sendImagePath(image_name, res) {
  
  fs.exists(`./uploads/products/${image_name}`, (exists) => {
    if (exists) {
      res.sendFile(path.resolve(`./uploads/products/${image_name}`));
    } else {
      res.status(404).send({
        message: '¡File not found!. ',
      });
    }
  });
  
}

function findImageById(product_id, res) {
  Product.findById(product_id, (err, product) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!product) {
      res.status(404).send({
        message: {
          text: 'Product not exists!',
          code: 404,
          error: '',
        },
      });
    } else {
      sendImagePath(product.product_image, res);
    }
  });
}

function searchProduct(req, res) {
  const { text } = req.params;
  const records_number = parseInt(req.params.records_number);
  const page = parseInt(req.params.page);
  const s = (page - 1) * records_number;
  let { sort_field } = req.params;
  let { sort_order } = req.params;
  if (!sort_field) {
    sort_field = 'created_date';
  }
  if (!sort_order) {
    sort_order = 'desc';
  }
  const query = {
    $or: [
      { name: { $regex: text, $options: 'i' } },
      { description: { $regex: text, $options: 'i' } },
    ],
  };
  const find = Product.find(query);
  find.limit(records_number).skip(s).sort([[sort_field, sort_order]]).exec((err, product) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!product) {
      res.status(404).send({
        message: {
          text: 'Product not exists!',
          code: 404,
          error: '',
        },
      });
    } else {
      res.status(200).send({
        Product: product,
      });
    }
  });
}

function update(id, params, res) {
  Product.findByIdAndUpdate(id, params, { new: true }, (err, productUpdated) => {
    if (err) {
      res.status(500).send({
        message: 'Error: The product don not save',
      });
    } else if (!productUpdated) {
      res.status(404).send({
        message: 'Product not exits!',
      });
    } else {
      res.status(200).send({
        product: productUpdated,
      });
    }
  });
}

function publishProduct(req, res) {
  const { productId } = req.params;
  const { userId } = req.params;
  Subscription.find({ user: userId, state: 'ACTIVE' }, (err, subscription) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    }
    if (!subscription[0]) {
      res.status(200).send({
        message: {
          text: 'Subscription not exists!',
          code: 404,
          error: '',
        },
      });
    } else {
      update(productId, { published: true }, res);
    }
  });
}

module.exports = {
  getProduct,
  getProducts,
  getFilePath,
  createProduct,
  updateProduct,
  deleteProduct,
  searchProduct,
  publishProduct,
};
