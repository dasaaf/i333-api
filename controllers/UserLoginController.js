const User = require('../models/user');

class UserLoginController {
  /**
   *
   * @param request
   * @param response
   * @returns {Promise<*>}
   */
  async me(request, response) {
    const { user } = request;
    User.findById(user, (err, userRegistered) => {
      if (err) {
        return response.status(202).json({
          message: {
            text: 'service not available, try later',
          },
        });
      }
      return response.json({ user: userRegistered });
    });
  }
}

module.exports = new UserLoginController();
