const request = require('request');
const { v4: uuidv4 } = require('uuid');
const User = require('../models/user');
const Mailer = require('./mailer');
const crypto = require('../tools/crypto');
const global = require('../global');
const S3Constants = require('../constants/S3Constants');
const fileManager = require('../services/fileManager');
const mongoose = require('mongoose');

function getUser(req, res) {
  const { id } = req.params;
  User.findById(id, { password: 0 }, (err, user) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can\'t complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!user) {
      res.status(404).send({
        message: {
          text: 'User not found',
          code: 404,
          error: '',
        },
      });
    } else {
      res.status(200).send({
        user,
      });
    }
  });
}

function getUsers(req, res) {
  User.find({}).select({ password: 0 }).sort('name').exec((err, users) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can\'t complete the operation.',
      });
    } else if (!users) {
      res.status(404).send({
        message: 'Users not found',
      });
    } else {
      res.status(200).send({
        users,
      });
    }
  });
}

function getExperts(req, res) {
  const records_number = parseInt(req.params.records_number);
  const page = parseInt(req.params.page);
  const s = (page - 1) * records_number;
  let { sort_field } = req.params;
  let { sort_order } = req.params;
  if (!sort_field) {
    sort_field = 'created_date';
  }
  if (!sort_order) {
    sort_order = 'desc';
  }
  User.find({ role: 'EXPERT', state: { $ne: 'DELETED' } }).exec((err, allUsers) => {
    if (!allUsers) {
      res.status(404).send({
        message: 'Users not found',
      });
    } else if (err) {
      res.status(500).send({
        message: 'Error: It is was can\'t complete the operation.',
      });
    } else {
      User.find({ role: 'EXPERT', state: { $ne: 'DELETED' } }).select({ password: 0 }).sort('name').limit(records_number)
        .skip(s)
        .exec((err, users) => {
          if (err) {
            res.status(500).send({
              message: 'Error: It is was can\'t complete the operation.',
            });
          } else if (!users) {
            res.status(404).send({
              message: 'Users not found',
            });
          } else {
            const pages_number = Math.ceil(allUsers.length / records_number);
            res.status(200).send({
              users,
              pages_number,
            });
          }
        });
    }
  });
}

function createUser(req, res) {
  const params = req.body;
  User.findOne({ email: params.email }, (err, user) => {
    if (!user) {
      var user = new User();
      user.first_name = params.first_name;
      user.last_name = params.last_name;
      user.photo = S3Constants.userDirDefault + S3Constants.userImgProfileDefault;
      user.email = params.email;
      user.title = 'UNDEFINED';
      user.professional_profile = 'UNDEFINED';
      user.academic_training = new Array();
      user.skills = new Array();
      user.laboral_experiences = new Array();
      user.references = new Array();
      user.password = crypto.encrypt(params.password);
      user.password_recovery = '';
      user.state = uuidv4();
      user.role = 'ENTREPRENEUR';
      user.birth_date = null;
      user.created_date = new Date();
      user.updated_date = new Date();
      user.deleted_date = null;
      user.save((err, userSaved) => {
        if (err) {
          res.status(500).send({
            message: 'Error: the user can not was registered',
            error: err,
          });
        } else {
          Mailer.sendMail(userSaved.email, `${global.SERVER_IP}:${global.SERVER_PORT}/api/activate-user/${user.state}/${userSaved._id}`, global.REGISTER_MAIL);
          res.status(200).send({
            user: userSaved,
            message: 'We have sent a mail for activate your account',
          });
        }
      });
    } else {
      Mailer.sendMail(userSaved.email, `${global.SERVER_IP}:${global.SERVER_PORT}/api/activate-user/${user.state}/${userSaved._id}`, global.REGISTER_MAIL);
      res.status(200).send({
        user: userSaved,
        message: 'We have sent a mail for activate your account',
      });
    }
  });
}

function getSkills(params) {
  const skillsArray = new Array();
  if (params.skills) {
    if (Array.isArray(params.skills)) {
      params.skills.forEach((e) => {
        skillsArray.push(JSON.parse(e));
      });
    } else {
      skillsArray.push(JSON.parse(params.skills));
    }
    return skillsArray;
  }
  return [];

}

function getAcademicTraining(params) {
  const academicTrainingArray = [];
  if (params.academic_training) {
    if (Array.isArray(params.academic_training)) {
      params.academic_training.forEach((e) => {
        academicTrainingArray.push(JSON.parse(e));
      });
    } else {
      academicTrainingArray.push(JSON.parse(params.academic_training));
    }
    return academicTrainingArray;
  }
  return [];

}

function getLaboralExperiences(params) {
  const laboralExperiencesArray = [];
  if (params.laboral_experiences) {
    if (Array.isArray(params.laboral_experiences)) {
      params.laboral_experiences.forEach((e) => {
        laboralExperiencesArray.push(JSON.parse(e));
      });
    } else {
      laboralExperiencesArray.push(JSON.parse(params.laboral_experiences));
    }
    return laboralExperiencesArray;
  }
  return [];

}

function getReferences(params) {
  const referencesArray = [];
  if (params.references) {
    if (Array.isArray(params.references)) {
      params.references.forEach((e) => {
        referencesArray.push(JSON.parse(e));
      });
    } else {
      referencesArray.push(JSON.parse(params.references));
    }
    return referencesArray;
  }
  return [];

}

async function updateUser(req, res) {
  try {
    const { id } = req.params;
    const params = req.body;
    params.skills = getSkills(params);
    params.academic_training = getAcademicTraining(params);
    params.laboral_experiences = getLaboralExperiences(params);
    params.references = getReferences(params);
    params.updated_date = new Date();
    const user = await User.findById(id);
    if (user) {
      let userUpdated = await User.findByIdAndUpdate(id, params, { new: true });
      if (userUpdated) {
        await uploadProfileImage(userUpdated, req.files.photo_file);
        await uploadCertificates(userUpdated, req.files.certificates);
        res.status(200).send({
          user: userUpdated,
        });
      }
    } else {
      res.status(404).send({
        message: {
          text: 'User not exists!',
          code: 404,
          error: '',
        },
      });
    }
  } catch (error) {
    console.log(error);
    res.send(error);
  }
}

function deleteUser(req, res) {
  const { id } = req.params;
  const params = req.body;
  User.findByIdAndRemove(id, params, (err, userDeleted) => {
    if (err) {
      res.status(500).send({
        message: 'Error: The user don\'t remove',
      });
    } else if (!userDeleted) {
      res.status(404).send({
        message: 'User not found',
      });
    } else {
      res.status(200).send({
        user: userDeleted,
      });
    }
  });
}

function activateAccount(req, res) {
  const userId = req.params.id;
  const { key } = req.params;
  User.findById(userId, (err, user) => {
    if (err) {
      res.status(500).send({
        message: 'Error: It is was can\'t complete the operation.',
      });
    } else if (!user) {
      res.status(404).send({
        message: 'User not found',
      });
    } else if (user.state === key) {
      User.findByIdAndUpdate(userId, { state: 'ACTIVE' }, (err, userUpdated) => {
        (userUpdated) => {
          res.status(200).send({
            user: userUpdated,
          });
        },
        (err) => {
          res.status(500).send({
            message: 'Error: It is was can not complete the operation.',
          });
        };
      });
      res.redirect('http://localhost:4200/');
      res.end();
    } else {
      res.status(200).send({
        message: 'The key of activated and the user does not match',
      });
    }
  });
}

async function getFilePath(req, res) {
  try {
    const user_id = req.params.id;
    const user = await User.findById(user_id);
    const image = await fileManager.get(S3Constants.bucket, user.photo);
    res.send(image.Body);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
}

/**
 *
 * @param req
 * @param res
 */
function searchExpert(req, res) {
  const { text } = req.params;
  const records_number = parseInt(req.params.records_number);
  const page = parseInt(req.params.page);
  const s = (page - 1) * records_number;
  let { sort_field } = req.params;
  let { sort_order } = req.params;
  if (!sort_field) {
    sort_field = 'created_date';
  }
  if (!sort_order) {
    sort_order = 'desc';
  }
  const filter = {
    $or: [
      { first_name: { $regex: text, $options: 'i' } },
      { last_name: { $regex: text, $options: 'i' } },
      { title: { $regex: text, $options: 'i' } },
      { professional_profile: { $regex: text, $options: 'i' } },
    ],
    $and: [{ state: { $ne: 'DELETED' }, role: { $ne: 'ENTREPRENEUR' }}],
  };
  const query = User.find(filter).select({ password: 0 });

  query.exec((err, user) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can not complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!user) {
      res.status(404).send({
        message: {
          text: 'User not exists!',
          code: 404,
          error: '',
        },
      });
    } else {
      res.status(200).send({
        user,
      });
    }
  });
}

function passwordRecovery(req, res) {
  const pass_token = uuidv4();
  User.findOneAndUpdate({ email: req.body.email }, { password_recovery: pass_token }, { new: true }).select({ password: 0 }).exec((err, userUpdated) => {
    if (err) {
      console.log(err);
    } else if (userUpdated) {
      Mailer.sendMail(userUpdated.email, `${global.URL_WEBSITE}/#/password-recovery?pass=${userUpdated.password_recovery}&user=${userUpdated._id}`, global.RESET_PASSWORD_MAIL);
      res.status(200).send({
        user: userUpdated,
        message: 'We have sent a mail for change password',
      });
    } else {
      res.status(200).send({
        message: 'The user does not exists',
      });
    }
  });
}

function verifyRecaptcha(req, res) {
  const token = req.body.captcha;
  const url = `https://www.google.com/recaptcha/api/siteverify?secret=${global.RECAPTCHA_SECRET_KEY}&response=${token}`;

  request(url, (error, response, body) => {
    if (body.success !== undefined && !body.success) {
      return res.json({ success: false });
    }
    res.json({ success: true });

  });
}
function changePass(data, res) {
  User.findOneAndUpdate({ _id: data.user_id }, { password: crypto.encrypt(data.new_pass), password_recovery: '' }, (err, userUpdated) => {
    if (err) {
      res.status(500).send({
        message: 'Error: The user don\'t save',
      });
    } else if (!userUpdated) {
      res.status(404).send({
        message: 'User not found',
      });
    } else {
      res.status(200).send({
        change: true,
      });
    }
  });
}

function resetPassword(req, res) {
  const data = req.body;
  User.findById(data.user_id, (err, user) => {
    if (err) {
      res.status(500).send({
        message: {
          text: 'Error: It is was can\'t complete the operation.',
          code: 500,
          error: err,
        },
      });
    } else if (!user) {
      res.status(404).send({
        message: {
          text: 'User not found',
          code: 404,
          error: '',
        },
      });
    } else if (user.password_recovery === data.pass_token
      || crypto.decrypt(user.password) === data.actual_pass) {
      changePass(data, res);
    } else {
      res.status(200).send({
        change: false,
      });
    }
  });
}

async function uploadProfileImage(user, image) {
  try {
    if (image) {
      const arrImage = image.name.split('.');
      const extImage = arrImage[arrImage.length - 1];
      const pathImage = `${S3Constants.userDir + user._id}/${uuidv4()}.${extImage}`;
      const objectS3 = await fileManager.put(S3Constants.bucket, pathImage, image.data);
      if (objectS3) {
        if (user.photo !== (S3Constants.userDirDefault + S3Constants.userImgProfileDefault)) {
          await fileManager.remove(S3Constants.bucket, user.photo);
        }
        return await User.findByIdAndUpdate(user._id, {photo: pathImage});
      }
    }
    return null;
  } catch (error) {
    console.log(error);
  }
}

  /**
   * Desde el frontend, llega un objeto si existe un elemento, o llega un arreglo si hay mas de un elemento,
 * por ese motivo la validación tiene que saber primero si lo que entra es un arreglo
 * @param user
 * @param files
 * @returns {Promise<null|*>}
 */
async function uploadCertificates(user, files) {
  if (files) {
    if (files instanceof Array) {
      for (const index in files) {
        await uploadCertificate(user, files[index]);
      }
    }
    else {
      await uploadCertificate(user, files);
    }
    return user;
  }
  return null;
}

async function uploadCertificate(user, file) {
  const arrCertificate = file.name.split('.');
  const extCertificate = arrCertificate[arrCertificate.length - 1];
  const idCertificate = uuidv4();
  const pathCertificate = `${S3Constants.userDir + user._id}/${idCertificate}.${extCertificate}`;
  const objectS3 = await fileManager.put(S3Constants.bucket, pathCertificate, file.data);
  if (objectS3) {
    return await User.findByIdAndUpdate(user._id,
      {$push: {
        certificates: {
          _id: mongoose.Types.ObjectId(),
          key: pathCertificate,
          name: arrCertificate[0]
        }}
      });
  }
  return null;
}

async function removeCertificate(req, res) {
  try {
    const { id_certificate } = req.params;
    const oId = new mongoose.Types.ObjectId(id_certificate);
    await User.findByIdAndUpdate(req.headers.user,
      {$pull: {
        certificates: {_id: oId}
        }});
    return res.status(200).send(await fileManager.remove(S3Constants.bucket, req.body.key));
  } catch (error) {
    throw new Error(error);
  }
}

async function downloadCertificate(req, res) {
  try {
    const certificate = await fileManager.get(S3Constants.bucket, req.headers.key);
    return res.status(200).send(certificate.Body);
  } catch (error) {
    throw new Error(error);
  }
}

module.exports = {
  activateAccount,
  createUser,
  deleteUser,
  getUser,
  getExperts,
  getFilePath,
  updateUser,
  searchExpert,
  passwordRecovery,
  resetPassword,
  verifyRecaptcha,
  uploadCertificate,
  removeCertificate,
  downloadCertificate
};
