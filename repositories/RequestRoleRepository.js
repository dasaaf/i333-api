const RequestRole = require('../models/requestRole');

class RequestRoleRepository {
  /**
   *
   * @param userId
   * @param roleName
   * @returns {Promise<*>}
   */
  static async findPending(userId, roleName) {
    const requestRole = await RequestRole.findOne({
      user_id: userId,
      state: 'PENDING',
      role_name: roleName,
      deleted_date: null,
    });
    return requestRole;
  }

  /**
   *
   * @param state
   * @param userId
   * @param roleName
   * @param review
   * @returns {Promise<*>}
   */
  static async create(state, userId, roleName, review = null) {
    const requestRole = await RequestRole.create({
      state,
      user_id: userId,
      role_name: roleName,
      review,
      created_date: new Date(),
      updated_date: null,
      deleted_date: null,
    });
    return requestRole;
  }
}

module.exports = RequestRoleRepository;
