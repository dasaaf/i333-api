const User = require('../models/user');

class UserRepository {
  /**
   *
   * @param id
   * @returns {Promise<*>}
   */
  static async findById(id) {
    const user = await User.findById(id);
    return user;
  }
}

module.exports = UserRepository;
