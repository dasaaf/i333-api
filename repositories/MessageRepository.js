const mongoose = require('mongoose');
const Message = require('../models/message');
const PaginationHelper = require('../tools/PaginationHelper');

class MessageRepository {
  /**
   *
   * @param userSenderId
   * @param userReceiverId
   * @param content
   * @param files
   * @returns {Promise<*>}
   */
  static async create(userSenderId, userReceiverId, content, files = null) {
    const message = await Message.create({
      content,
      created_date: new Date(),
      updated_date: null,
      deleted_date: null,
      received_date: null,
      read_date: null,
      files,
      user_sender: userSenderId,
      user_receiver: userReceiverId,
    });
    return message;
  }

  /**
   *
   * @param id
   * @returns {Promise<*>}
   */
  static async findById(id) {
    const message = await Message.findOne({ _id: id });
    return message;
  }

  /**
   *
   * @param userIdA
   * @param userIdB
   * @param page
   * @return {Promise<*>}
   */
  static async historyBetweenUsersWithPagination(userIdA, userIdB, page) {
    const options = {
      page: page || 1,
      limit: PaginationHelper.defaultLimit,
      customLabels: PaginationHelper.customLabels,
    };
    const query = { $or: [
      {
        $and: [
          { user_sender: mongoose.Types.ObjectId(userIdA) },
          { user_receiver: mongoose.Types.ObjectId(userIdB) },
        ],
      },
      {
        $and: [
          { user_sender: mongoose.Types.ObjectId(userIdA) },
          { user_receiver: mongoose.Types.ObjectId(userIdB) },
        ],
      },
    ] };
    const messages = await Message.paginate(query, options);
    return messages;
  }
}

module.exports = MessageRepository;
