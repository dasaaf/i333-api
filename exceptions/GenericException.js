const HttpCodes = require('../controllers/HttpCodes');

class GenericException {
  /**
     *
     * @param message
     * @returns {Error}
     */
  constructor(message) {
    const error = new Error(message);
    error.status = this.status();
    return error;
  }

  /**
     *
     * @returns {string}
     */
  status() {
    return HttpCodes['Bad Request'];
  }
}

module.exports = GenericException;
