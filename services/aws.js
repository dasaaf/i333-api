const AWS = require('aws-sdk');
const global = require('../global');

const s3 = new AWS.S3({
  accessKeyId: global.ACCESS_KEY_ID,
  secretAccessKey: global.SECRET_ACCESS_KEY,
});

module.exports = {
  s3,
};
