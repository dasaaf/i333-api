const Request = require('../components/Request');

class ImageFromExternalUrl {
  /**
   *
   * @param imageUrl
   * @returns {Promise<{data: *, type: *}>}
   */
  static async upload(imageUrl) {
    try {
      const response = await Request.getImageExternalServer(true, imageUrl, null);
      const { data, headers } = response;
      const imageBase64 = `data:${headers['content-type']};base64,${Buffer.from(data, 'binary').toString('base64')}`;
      const contentType = headers['content-type'].split('/');
      const imageDecoded = Buffer.from(imageBase64.replace(/^data:image\/\w+;base64,/, ''), 'base64');
      const extension = contentType[1];
      const image = {
        data: imageDecoded,
        extension,
      };
      return image;
    } catch (error) {
      console.log('error', { imageUrl, error });
      return null;
    }
  }
}

module.exports = ImageFromExternalUrl;
