const mongoose = require('mongoose');
const http = require('http');

const global = require('./global');

global.path = require('path');

global.rootPath = global.path.join(__dirname, '../');

global.use = function (path) {
  // eslint-disable-next-line global-require,import/no-dynamic-require
  return require(`${rootPath}${path}`);
};

const app = require('./app');

const server = http.Server(app);

require('./config/SocketIO')(server);

mongoose.set('useFindAndModify', false);

if (global.MONGO_PASSWORD != '') {
  mongoose.connect(`mongodb://${global.MONGO_USER}:${global.MONGO_PASSWORD}@${global.MONGO_IP}:${global.MONGO_PORT}/${global.MONGO_DB}`, { useNewUrlParser: true }, (err, res) => {
    if (err) {
      throw err;
    }
    console.log('Mongo Connection: ON');
    server.listen(global.SERVER_PORT, () => {
      console.log(`API Server Listen: ON. http://${global.MONGO_IP}:${global.SERVER_PORT}`);
    });
  });
}
else {
  mongoose.connect(`mongodb://${global.MONGO_IP}:${global.MONGO_PORT}/${global.MONGO_DB}`, { useNewUrlParser: true }, (err, res) => {
    if (err) {
      throw err;
    }
    console.log('Mongo Connection: ON');
    server.listen(global.SERVER_PORT, () => {
      console.log(`API Server Listen: ON. http://${global.MONGO_IP}:${global.SERVER_PORT}`);
    });
  });
}
